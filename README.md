How it works:

0. Make sure that you have the BLEClient Android app open and check that the phone is paired
while also connected to IMU's wifi, on hololens.
1. You run the .exe on hololens
2. Wait a little, you'll see a black screen and after that, you'll see a car, with the GPS Coordinates, whose transform
is being controlled by the IMU (I'm just setting the IMU as a parent of the car's gameobject, when its connected)

ToDo:
1. I havent finished the GPS distance measurement, i.e. the display of how far is a pair of GPS coordinates from another
2. To render this objects in a real coordinate system.

How to develop on Editor:

Due to the fact that I've changed libraries (i.e. the DLLs) in case you leave the Wi-Fi library (you can find this folder at
Assets/NGIMU) you wont be able to see any changes related to IMU transform due to the editor not taking into account the
WiFi connection.

You'll have to replace it with the original library which can be found on NGIMU's Unity example, which enables the serial connection. Thus, you have to connect via USB the IMU to the PC.

Just keep in mind to not delete the Wi-Fi folder so you can replace it again before building the solution, when ready, for Hololens.

In order to avoid messing with other repo's I created this one.

P.S. I havent added the MRTK package yet. In case you are not bored, feel free to do that too.