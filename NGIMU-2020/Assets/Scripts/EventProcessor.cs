﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventProcessor : MonoBehaviour
{
    
    private System.Object _queueLock = new System.Object();
    List<byte[]> _queuedData = new List<byte[]>();
    List<byte[]> _processingData = new List<byte[]>();

    [Header("User Interface")]
    public TextMesh LatitudeText;
    public TextMesh LongitudeText;
    public TextMesh errorText;

    public void QueueData(byte[] data)
    {
        lock (_queueLock)
        {
            _queuedData.Add(data);
        }
    }

    void Update()
    {
        MoveQueuedEventsToExecuting();
        while (_processingData.Count > 0)
        {
            var byteData = _processingData[0];
            _processingData.RemoveAt(0);
            try
            {
                var gpsData = GPSCoordinatesData.ParseByteData(byteData);
                LatitudeText.text = "Latitude: " + gpsData.Latitude.ToString();
                LongitudeText.text = "Longitude: " + gpsData.Longitude.ToString();
               
            }
            catch (Exception e)
            {
                errorText.text = "Error: " + e.Message;
            }
        }
    }

    private void MoveQueuedEventsToExecuting()
    {
        lock (_queueLock)
        {
            while (_queuedData.Count > 0)
            {
                byte[] data = _queuedData[0];
                _processingData.Add(data);
                _queuedData.RemoveAt(0);
            }
        }
    }
}
