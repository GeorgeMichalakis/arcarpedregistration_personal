﻿using UnityEngine;


#if WINDOWS_UWP
using Windows.Devices.Bluetooth.Advertisement;
using System.Runtime.InteropServices.WindowsRuntime;
#endif

public class ReceiverBLE : MonoBehaviour
{

#if WINDOWS_UWP
    BluetoothLEAdvertisementWatcher watcher;
    public static ushort BEACON_ID = 1775;
#endif
    public TMPro.TextMeshProUGUI errorText;
    public EventProcessor eventProcessor;
    public TMPro.TextMeshProUGUI advertiserNameText;

    void Awake()
    {
#if WINDOWS_UWP
        watcher = new BluetoothLEAdvertisementWatcher();


        var manufacturerData = new BluetoothLEManufacturerData
        {
        CompanyId = BEACON_ID
        };

        watcher.AdvertisementFilter.Advertisement.ManufacturerData.Add(manufacturerData);

        watcher.Received += Watcher_Received;

        watcher.Start();

#endif
    }
#if WINDOWS_UWP
    private async void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
    {
        ushort identifier = args.Advertisement.ManufacturerData[0].CompanyId;
        byte[] data = args.Advertisement.ManufacturerData[0].Data.ToArray();
        // Updates to Unity UI don't seem to work nicely from this callback so just store a reference to the data for later processing.
        eventProcessor.QueueData(data);
    }
#endif
}
