﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class GPSCoordinatesData
{
    public double Latitude;
    public double Longitude;
    public float Heading;
    public float Speed;

    public static GPSCoordinatesData ParseByteData(byte[] data)
    {
        GPSCoordinatesData gpsCoordinates =new GPSCoordinatesData();
        gpsCoordinates.Latitude = BitConverter.ToDouble(data, 0);
        gpsCoordinates.Longitude = BitConverter.ToDouble(data, 8);
        gpsCoordinates.Heading = BitConverter.ToSingle(data, 16);
        gpsCoordinates.Speed = BitConverter.ToSingle(data, 20);

        return gpsCoordinates;
    }

    public override string ToString()
    {
        string latitudeFormat = string.Format("{0:0.00} AfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfA'A?N", (Latitude > 0) ? Latitude : -Latitude);
        string longitudeFormat = string.Format("{0:0.00} AfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfA'A?N", (Longitude > 0) ? Longitude : -Longitude);

        return string.Format("Latitude: {0}, Longitude: {1}, Heading: {2:0.00}AfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfAfA'A?, Speed: {3:0.00} knots",
            latitudeFormat, longitudeFormat, Heading, Speed);
    }
}
