﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutomateTheClick : MonoBehaviour
{
    public bool ready = false;
    public GameObject sphere;
    public GameObject NGIMU;
    void Start()
    {
        transform.GetComponent<Button>().onClick.Invoke();
        ready = !ready;
        sphere.transform.SetParent(NGIMU.transform);
    }
}
